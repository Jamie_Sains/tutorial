﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial
{
    class Program
    {
        static void Main(string[] args)
        { //this is a comment
            Console.WriteLine("Hello World!");
            Console.WriteLine("I will Print on a new line.");

            Console.Write("Hello World!");
            Console.Write("I Will write on thw same line.");
            

            String name = "John";
            Console.WriteLine(name);

            int myNum = 15;
            Console.WriteLine(myNum);

            double myDoubleNum = 5.99D;
            char myLletter = 'd';
            bool myBool = true;
            string myText = "Hello";

            Console.WriteLine("Hello" + name);

            string firstName = " John";
            string lastName = "Doe";

            String fullName = firstName + lastName;
            Console.WriteLine(fullName);

            int x = 5;
            int y = 6;
            Console.WriteLine(x + y); //Print the value of x + y

            int z = 50;
            Console.WriteLine(x + y + z);

            int minutesPerHour = 60;
            int m = 60;

            Console.ReadLine();
        }
    }
}
